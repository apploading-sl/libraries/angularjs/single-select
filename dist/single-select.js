(function () { 
"use strict";
angular
  .module('materialism.single-select', [])
  .directive('singleSelect', singleSelect);

function singleSelect() {
  var directive = {
    restrict: 'E',
    templateUrl: 'materialism-single-select.html',
    scope: {
      selectedItem: '=',
      items: '=',
      name: '@',
      disabled: '=',
      filter: '&',
      onSelect: '=?',
      placeholder: '@',
      searchEnabled: '=?',
    },
    controller: SingleSelectController,
    controllerAs: 'vm',
    bindToController: true,
  };

  return directive;
}

SingleSelectController.$inject = [];

function SingleSelectController() {
  var vm = this;

  setDefault();

  function setDefault() {
    vm.searchEnabled = vm.searchEnabled || false;
    if (vm.selectedItem === null && vm.items && vm.items.length) {
      vm.selectedItem = vm.items[0].id;
    }

    if (typeof vm.filter === 'undefined') {
      vm.filter = function(item) {
        return item;
      };
    }
  }

  vm.onSelectEvent = function(item) {
    if (vm.onSelect && typeof vm.onSelect === 'function') {
      vm.onSelect(item);
    }
  };
}

}());
angular.module('materialism.single-select').run(['$templateCache', function($templateCache) {$templateCache.put('materialism-single-select.html','<div class="form-group filled"><label class="control-label">{{ vm.name || vm.label }}</label><ui-select ng-model="vm.selectedItem" ng-disabled="vm.disabled" theme="select2" search-enabled="vm.searchEnabled" on-select="vm.onSelectEvent($item)"><ui-select-match placeholder="{{ vm.placeholder || \'-\' }}">{{ $select.selected.name || $select.selected.label }}</ui-select-match><ui-select-choices repeat="item.id as item in vm.items | filter: (vm.searchEnabled ? $select.search : vm.filter())"><div ng-bind-html="(item.name || item.label) | highlight: $select.search"></div></ui-select-choices></ui-select></div>');}]);