angular
  .module('materialism.single-select', [])
  .directive('singleSelect', singleSelect);

function singleSelect() {
  var directive = {
    restrict: 'E',
    templateUrl: 'materialism-single-select.html',
    scope: {
      selectedItem: '=',
      items: '=',
      name: '@',
      disabled: '=',
      filter: '&',
      onSelect: '=?',
      placeholder: '@',
      searchEnabled: '=?',
    },
    controller: SingleSelectController,
    controllerAs: 'vm',
    bindToController: true,
  };

  return directive;
}

SingleSelectController.$inject = [];

function SingleSelectController() {
  var vm = this;

  setDefault();

  function setDefault() {
    vm.searchEnabled = vm.searchEnabled || false;
    if (vm.selectedItem === null && vm.items && vm.items.length) {
      vm.selectedItem = vm.items[0].id;
    }

    if (typeof vm.filter === 'undefined') {
      vm.filter = function(item) {
        return item;
      };
    }
  }

  vm.onSelectEvent = function(item) {
    if (vm.onSelect && typeof vm.onSelect === 'function') {
      vm.onSelect(item);
    }
  };
}
