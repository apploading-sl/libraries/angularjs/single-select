var gulp = require('gulp');
var streamqueue = require('streamqueue');
var $ = require('gulp-load-plugins')();

gulp.task('default', ['build']);

gulp.task('build', ['scripts']);

gulp.task('scripts', function() {
  var buildTemplates = function() {
    return gulp
      .src('src/**/*.html')
      .pipe(
        $.minifyHtml({
          empty: true,
          spare: true,
          quotes: true,
        })
      )
      .pipe($.angularTemplatecache({ module: 'materialism.single-select' }));
  };

  var buildLib = function() {
    return gulp
      .src(['src/common.js', 'src/*.js'])
      .pipe(
        $.plumber({
          errorHandler: handleError,
        })
      )
      .pipe($.concat('single-select_without_templates.js'))
      .pipe($.header('(function () { \n"use strict";\n'))
      .pipe($.footer('\n}());'))
      .pipe(gulp.dest('temp'));
  };

  return streamqueue({ objectMode: true }, buildLib(), buildTemplates())
    .pipe(
      $.plumber({
        errorHandler: handleError,
      })
    )
    .pipe($.concat('single-select.js'))
    .pipe(gulp.dest('dist'))
    .pipe($.sourcemaps.init())
    .pipe($.uglify({ preserveComments: 'some' }))
    .pipe($.concat('single-select.min.js'))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('dist'));
});

var handleError = function(err) {
  console.log(err.toString());
  this.emit('end');
};
